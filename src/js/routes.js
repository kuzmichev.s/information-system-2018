const Index = () => import(/* webpackChunkName: "index" */ '~/pages/Index').then(m => m.default || m);

export default [
    {
        path: '/',
        name: 'index',
        component: Index,
    },
];